<?php

ob_start(); // Start buffering output

$id = $_GET['student_id'];
$username = $_GET['username'];
$password = $_GET['password'];

// You can temporarily remove or comment out this line to avoid the header issue
// echo "$id, $username ,$password";

if ($id) {

    include "../../connection/connection_db.php";

    $sql = "DELETE FROM students WHERE student_id =?";
    $stmt = $conn->prepare($sql);
    $stmt->execute([$id]);

    $sql = "DELETE FROM users WHERE username =?";
    $stmt = $conn->prepare($sql);
    $stmt->execute([$username]);

    $sm = "Student deleted successfully";
    header("Location: ../../html/students.php?error=$sm");
    exit;
} else {
    $em = "Invalid student or ClassStatus";
    header("Location: ../../html/students.php?error=$em");
    exit;
}

ob_end_flush(); // Send output buffer and turn off output buffering

?>
