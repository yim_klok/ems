<?php
session_start();
ob_start(); // Start buffering output
// Assuming $_POST is sanitized properly before use
$email_username = $_POST['email-username'] ?? ''; // Using null coalescing operator
$pwd = $_POST['password'] ?? '';


if (!empty($email_username) && !empty($pwd)) {

    include "../../connection/connection_db.php";

    $sql_query = preg_match('/^\S+@\S+\.\S+$/', $email_username) ?
        "SELECT * FROM users WHERE email = ?" :
        "SELECT * FROM users WHERE username = ?";

    $stmt = $conn->prepare($sql_query);
    $stmt->execute([$email_username]);

    if ($stmt->rowCount() == 1) {

        $user = $stmt->fetch(); // Get data from table users

        // Assigning variables for readability
        $uname  = $user['username'];
        $email  = $user['email'];
        $hashedPwd = $user['pwd'];
        $role   = $user['roles'];

        // Password verification
        if (password_verify($pwd, $hashedPwd)) {
            // Set session variables
            $_SESSION['role'] = $role;
            $_SESSION['username'] = $uname;
            $_SESSION['fname'] = $user['firstname'];
            $_SESSION['lname'] = $user['lastname'];

            // Set a cookie for the role, expires in 1 hour
            setcookie("role", $role, time() + 3600, "/");

            header("Location: ../../");
            exit;
        } else {
            // Password is incorrect
            echo "Message: Password incorrect";
            // Redirect or handle error
        }
    } else {
        echo "No data found for the provided credentials.";
        // Redirect or handle error
    }
} else {
    $em = "Email or Username and password not entered";
    header("Location: ../auth-login-basic.php?error=" . urlencode($em));
}
ob_end_flush(); // Send output buffer and turn off output buffering
