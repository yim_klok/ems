<?php

    $localhost  = "db";
    $db_name    = "ems_db";
    $db_username   = "root";
    $db_password   = "secret";
    $port       = 3306;
    // $localhost  = "localhost";
    // $db_name    = "ems_db";
    // $db_username   = "root";
    // $db_password   = "klok!=thida?love:hate";
    // $port       = 2002;

    try {
        $conn = new PDO("mysql:host=$localhost;port=$port;dbname=$db_name", $db_username, $db_password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
        exit;
    }

?>
