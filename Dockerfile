# Use an official PHP runtime as a parent image with Apache
FROM php:8.0-apache

# Fix the Apache "Could not reliably determine the server's fully qualified domain name" warning
RUN echo 'ServerName localhost' >> /etc/apache2/apache2.conf

WORKDIR /var/www/html

# Install MySQL extensions for PHP
RUN docker-php-ext-install mysqli pdo pdo_mysql && docker-php-ext-enable mysqli

# Update and upgrade packages - consider adding '-q' for quieter output, and '--no-install-recommends' to minimize image size
RUN apt-get update && apt-get upgrade -y

# Copy your source code to the Apache document root
COPY . /var/www/html/

# Expose port 80 to access your app
EXPOSE 80
